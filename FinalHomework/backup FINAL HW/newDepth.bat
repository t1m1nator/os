

@echo off
rem check if we can only create folder.bat ONLY when there are no subdirectories in the current file
rem IE only create folders at the current bottom of a tree 
set back=%cd%
set foldername=%2
if "%2"=="" (
set foldername="HideMyStuff"
)
set depth=%1

for /l %%d in (1,1,%depth%) do (

	for /f %%g in ('dir /b /s /a:d') do (
	echo %%g
	cd "%%g"
	if not exist "%%g\%foldername%1" (
		call %back%\folder.bat %foldername%
		)
	)
cd %back%
)
rem c:\Users\tim\Documents\VGTU\OperatingSystem\FinalHomework\CMD\hide.bat