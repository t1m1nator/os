

@echo off
rem for every subdirectory up till #depth check if any folders exist, and if not, call folder.bat to create some
rem then change directory to top and try again?
set back=%cd%
set foldername=%2
if "%2"=="" (
set foldername="HideMyStuff"
)
set depth=%1

for /l %%d in (1,1,%depth%) do (

	for /f %%g in ('dir /b /s /a:d') do (
	echo %%g
	cd "%%g"
	if not exist "%%g\%foldername%1" (
		call %back%\folder.bat %foldername%
		)
	)
cd %back%
)
rem c:\Users\tim\Documents\VGTU\OperatingSystem\FinalHomework\CMD\hide.bat