@echo off
title HideMyStuff
echo.
:menu
cls
for /l %%f IN (1,1,7) DO echo HideMyStuff           HideMyStuff    HideMyStuffHideMyStuffHideMyStuff
for /l %%f IN (1,1,7) DO echo HideMyStuffHideMyStuffHideMyStuff               HideMyStuff 
for /l %%f IN (1,1,7) DO echo HideMyStuff           HideMyStuff    HideMyStuffHideMyStuffHideMyStuff 
echo.
for /l %%f IN (1,1,2) DO echo HideMyStuffHideMyStuffHide           HideMyStuffHideMyStuffHideMyStuff
for /l %%f IN (1,1,2) DO echo HideMyStuffHideMyStuffHideMy         HideMyStuffHideMyStuffHideMyStuff
for /l %%f IN (1,1,2) DO echo HideMyStuffHideMyStuffHideMyStuff    HideMyStuff
for /l %%f IN (1,1,2) DO echo HideMyStuff           HideMyStuff    HideMyStuff
for /l %%f IN (1,1,4) DO echo HideMyStuff           HideMyStuff    HideMyStuffHideMyStuffHideMyStuff
for /l %%f IN (1,1,2) DO echo HideMyStuff           HideMyStuff    HideMyStuff
for /l %%f IN (1,1,2) DO echo HideMyStuffHideMyStuffHideMyStuff    HideMyStuff
for /l %%f IN (1,1,2) DO echo HideMyStuffHideMyStuffHideMy         HideMyStuffHideMyStuffHideMyStuff
for /l %%f IN (1,1,2) DO echo HideMyStuffHideMyStuffHide           HideMyStuffHideMyStuffHideMyStuff
echo.
echo Welcome
rem MAIN MENU
echo Please select one: 
echo 1)Hide a file
echo 2)Find your file
echo 3)Delete HideMyStuff folder network
echo 4)Quit
choice /c 1234 /n
IF errorlevel 4 goto bye
IF errorlevel 3 goto purge
IF errorlevel 2 goto search
IF errorlevel 1 goto hide

:hide
rem receives input from user (depth to hide) and checks for validity
cls
echo.
echo Hide Your Stuff
echo This will create a folder subsystem and randomly hide your file
set/p amount=Enter Minimum Depth to Hide:  
echo.
set/p limit=Enter Total Depth: 
if (%limit%) LSS (%amount%) (
call :wrongdepth 
rem checks to make sure max depth is bigger than min depth
)
:check1
rem Asks for file to hide and makes sure it exists
echo.
set/p filename="Enter File to Hide (provide the path if not in the same folder as this .bat): "
echo.
IF NOT EXIST %filename% (call :fileError)
:check2
rem makes initial hide folder, calls bat to create the network
cls 
md hide
echo "Creating Folder Network"
echo "Depth: "%limit%
call %cd%\newDepth.bat %limit%
echo "..."
echo "Network Complete"
timeout /t 4
rem calls bat to hide the file in a random location
call %cd%\cleanPath.bat %amount% %limit% HideMyStuff %filename%
pause
goto menu
:search
rem finds the hidden file 
cls
set/p Find="Name of the file to find (with extension .txt, .bat ect.): "
where /r %cd% %Find%
echo "Would you like to retrieve the file?(Y/N) "
choice /n
if errorlevel 2 goto menu
if errorlevel 1 goto move
:move
set/p destin="Where would you like to move the file?(exact path) "
for /f %%i in ('where /r %cd% %Find%') do set filep=%%i
move %filep% %destin%
pause
goto menu
:purge
cls
echo "Are you sure you want to delete the folder network? (any files hidden will be deleted) Y/N "
choice /n
if errorlevel 2 goto menu
if errorlevel 1 goto delete
:delete
rmdir /s /q %cd%\hide

goto menu
:bye
cls
echo Goodbye :)
pause
exit /b
:fileError
echo Error, file not found!
set/p filename="Enter File to Hide (provide the path if not in the same folder as this .bat): "
IF NOT EXIST %filename% (call :fileError)
goto check2

:wrongdepth
echo Error, value for Depth is less than amount!
set/p limit=Enter Total Depth: 
if (%limit%) LSS (%amount%) (
call :wrongdepth
)
goto check1



rem TO FIX: 1) hides the file one to high (depth is 5, hidden in 4) 
rem TO FIX: 2)If hiding multiple files it creates new networks in the old one (complex) should use old one and add if needs more depth
rem TO FIX: 3)clean up min depth? clean up menus and feedback
