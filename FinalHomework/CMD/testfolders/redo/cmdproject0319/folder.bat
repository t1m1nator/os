@echo off
rem argument 1 is current folder name
rem creates a random (up to 5 now) number of folders of name (argument)foldernumber (ect 101, 102, 103)

set foldername=%1

set/a foldernumber=(%random%*5/32768)+1

for /l %%f in (1,1,%foldernumber%) do (
md %foldername%%%f
)