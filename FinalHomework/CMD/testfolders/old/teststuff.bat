
@echo off
echo %0
echo.
echo %~dp0 

rem check file name creation with variables

rem setlocal EnableDelayedExpansion
set/a lim=%1
set/a folders=(%random%*%lim%/32768)+1 rem number of folders per layer
echo %folders%
echo startloop
	for /L %%f in (1,1,%folders%) do ( rem loop for each folder in depth x
		echo %%f
		for /L %%d in (1,1,3) do (
		mkdir %%f%%d
		)
	)
	rem works as of now... creates multiple files of combined name